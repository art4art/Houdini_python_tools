cur_nodes = hou.selectedNodes()

if not hou.node("/obj/ABC"):
    node = hou.node("obj/")
    geo = node.createNode("geo", "ABC")
    geo.moveToGoodPosition()


else:
    geo = hou.node("/obj/ABC")

    for abc in cur_nodes:
        parent = abc.parent()
        rop_name = parent.name()
        name = abc.name()
        alembic = geo.createNode('alembic', name)
        path_to_node = abc.path()
        path_to_file = abc.evalParm("filename")
        rel_path = '`' + 'chs("' + '../../' + rop_name + '/' + name + '/' + 'filename' + '")' + '`'
        alembic.parm('fileName').set(rel_path)
        alembic.moveToGoodPosition()




