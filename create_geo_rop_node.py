sel_nodes = hou.selectedNodes()

if not hou.node("/obj/SIM_PASSES"):
    node = hou.node("obj/")
    geo_rop = node.createNode("ropnet", "SIM_PASSES")
    geo_rop.moveToGoodPosition()


else:
    geo_rop = hou.node("obj/SIM_PASSES")
    for n in sel_nodes:
        name = n.name()
        if not hou.node("obj/SIM_PASSES/" + name):
            geo_render = geo_rop.createNode("geometry", name)
            geo_render.moveToGoodPosition()
            path = n.path()
            geo_render.parm("soppath").set(path)
            geo_render.parm('trange').set(1)
        else:
            pass










