#This code will helps you to merge bunch of nodes in fbx subnet in to one geo node for further steps

sel_nodes = hou.selectedNodes()
main_geo = hou.node("/obj/").createNode("geo", "main_geo")
main_merge = main_geo.createNode("merge", "merge_")
main_merge.moveToGoodPosition()
main_merge.setDisplayFlag(1)

for node in sel_nodes:
    child = node.children()
    merge = main_geo.createNode("object_merge", "import_")
    path = node.path()
    merge.parm("objpath1").set(path)
    merge.parm("xformtype").set(1)
    main_merge.setNextInput(merge)

    merge.moveToGoodPosition()