#This code allows you to split your incoming geo on sepatated blast nodes by name attribute

if len(hou.selectedNodes()) == 0:
    hou.ui.DisplayMessage("Please select a node")


else:
    curNode = hou.selectedNodes()[0]

    nameAttr = curNode.geometry().findPrimAttrib("name")

    for pt in nameAttr:
        blast = curNode.createOutputNode("blast", curNode.name())
        blast.moveToGoodPosition()
        blast.parm("group").set('@name=' + nameAttr + '*')
        blast.parm("negate").set(1)

